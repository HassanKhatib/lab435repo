#ifndef GAME1SCENE_H
#define GAME1SCENE_H

#include <QObject>
#include <QWidget>
#include <QtWidgets>


class game1scene : public QWidget
{
    Q_OBJECT
public:
    explicit game1scene(QJsonValue user = QJsonValue::Null,QWidget *parent = nullptr);
    QJsonValue loggedInUser;

    int bigShipI;
    int smallShipI;
    int smallShipJ;
    int mediumShipI;
    int mediumShipJ;
    QString locBigShip1;
    QString locBigShip2;
    QString locBigShip3;
    QString locBigShip4;
    QString locMediumShip1;
    QString locMediumShip2;
    QString locSmallShip;


private:
    void initialize();
    void addButtons();
    void styleButton(QPushButton *btn);
    void styleButtons();
    void randomizeShips();
    void smallShipPos();
    void mediumShipPos();
    void largeShipPos();

    void setInternalLayout();


    void openQuestionDialog();
    void spawnVisual();

    QPushButton *a11,*a12,*a13,*a14,*a21,*a22,*a23,*a24,*a31,*a32,*a33,*a34,*a41,*a42,*a43,*a44;

    QList<QPushButton*> *listButtons;

    QGridLayout *internalGridLayout;
signals:


public slots:
    void clickButton();

};

#endif // GAME1SCENE_H
