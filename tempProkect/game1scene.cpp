#include "game1scene.h"

#include <QTimer>


game1scene::game1scene(QJsonValue user,QWidget *parent) : QWidget(parent)
{


    loggedInUser = user;
    listButtons = new QList<QPushButton*>;


    internalGridLayout = new QGridLayout();

    initialize();

    addButtons();
    setInternalLayout();
    QWidget *w = new QWidget();
    w->setLayout(internalGridLayout);
    w->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    QVBoxLayout* containerLayout = new QVBoxLayout;
    containerLayout->addWidget(w, 0);
    QGridLayout *hboxLayout = new QGridLayout();
    hboxLayout->addItem(new QSpacerItem(605,0),1,0);
    hboxLayout->addItem(new QSpacerItem(0,228),0,1);
    hboxLayout->addItem(containerLayout,1,1);
    setLayout(hboxLayout);
    show();

}

void game1scene::initialize(){
    setFixedSize(1114,802);
    adjustSize();
    move(QApplication::desktop()->screen()->rect().center() - this->rect().center());
    setWindowTitle("Game One");
    QPixmap bkgnd(":/Game1Files/game1backround.png");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    setPalette(palette);


}


void game1scene::styleButton(QPushButton *btn) {
    btn->setContentsMargins(0,0,0,0);
    btn->setContentsMargins(0,0,0,0);
    QSize btnSize = QSize(90,90);
    btn->setFixedSize(btnSize);
    btn->setStyleSheet("QPushButton{background: rgba(255,255,255,0);"
                            "color:white;}");
}


void game1scene::addButtons(){
    randomizeShips();
    smallShipPos();
    mediumShipPos();
    largeShipPos();
    qInfo()<<locSmallShip +"small";
    qInfo()<<locMediumShip1+"medium";
    qInfo()<<locBigShip1+"large";


    a11 = new QPushButton("a11");
    a12 = new QPushButton("a12");
    a13 = new QPushButton("a13");
    a14 = new QPushButton("a14");
    a21 = new QPushButton("a21");
    a22 = new QPushButton("a22");
    a23 = new QPushButton("a23");
    a24 = new QPushButton("a24");
    a31 = new QPushButton("a31");
    a32 = new QPushButton("a32");
    a33 = new QPushButton("a33");
    a34 = new QPushButton("a34");
    a41 = new QPushButton("a41");
    a42 = new QPushButton("a42");
    a43 = new QPushButton("a43");
    a44 = new QPushButton("a44");
    listButtons->append(a11);
    listButtons->append(a12);
    listButtons->append(a13);
    listButtons->append(a14);
    listButtons->append(a21);
    listButtons->append(a22);
    listButtons->append(a23);
    listButtons->append(a24);
    listButtons->append(a31);
    listButtons->append(a32);
    listButtons->append(a33);
    listButtons->append(a34);
    listButtons->append(a41);
    listButtons->append(a42);
    listButtons->append(a43);
    listButtons->append(a44);
    for(QPushButton * button: *listButtons) {
        QString btnText = button->text();
        QObject::connect(button, SIGNAL(clicked(bool)), this, SLOT(clickButton()));
        styleButton(button);
    }

}


void game1scene::randomizeShips() {

    bigShipI = QRandomGenerator::global()->bounded(1,5);


    mediumShipI = QRandomGenerator::global()->bounded(1,5);
    while(mediumShipI==bigShipI){
    mediumShipI = QRandomGenerator::global()->bounded(1,5);
    }
    mediumShipJ = QRandomGenerator::global()->bounded(1,4);



    smallShipI = QRandomGenerator::global()->bounded(1,5);
    while(smallShipI==bigShipI){
    smallShipI = QRandomGenerator::global()->bounded(1,5);
    }
    smallShipJ = QRandomGenerator::global()->bounded(1,5);

    if (smallShipI==mediumShipI){
        if(smallShipJ==mediumShipJ||smallShipJ==mediumShipJ+1){
            while(true){
                smallShipI = QRandomGenerator::global()->bounded(1,5);
                while(smallShipI==bigShipI){
                smallShipI = QRandomGenerator::global()->bounded(1,5);
                }
                smallShipJ = QRandomGenerator::global()->bounded(1,5);

                if(!(smallShipJ==mediumShipJ)){
                    if(!(smallShipJ==mediumShipJ+1)){
                        break;}

                }
            }
        }



    }

}

void game1scene::smallShipPos() {
    QString strSSI = QString::number(smallShipI);
    QString strSSJ = QString::number(smallShipJ);
    locSmallShip = "a" + strSSI+strSSJ;
}
void game1scene::mediumShipPos() {
    QString strMSI = QString::number(mediumShipI);
    QString strMSJ = QString::number(mediumShipJ);
    QString strMSJ2 = QString::number(mediumShipJ+1);
    locMediumShip1 = "a" + strMSI+strMSJ;
    locMediumShip2 = "a" + strMSI+strMSJ2;
}

void game1scene::largeShipPos() {
    QString strBS = QString::number(bigShipI);
    locBigShip1 = "a" + strBS +QString::number(1);
    locBigShip2 = "a" + strBS +QString::number(2);
    locBigShip3 = "a" + strBS +QString::number(3);
    locBigShip4 = "a" + strBS +QString::number(4);

}

void game1scene::clickButton() {
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    bool sB = false;
    QString btnText = buttonSender->text();
    QStringList ll =btnText.split("");
    QString strRow = ll[2];
    QString strCol = ll[3];
    int btnRow = strRow.toInt();
    int btnCol = strCol.toInt();
    if (btnText == locSmallShip ||
            btnText == locMediumShip1 || btnText ==locMediumShip2||
            btnText == locBigShip1 || btnText ==locBigShip2 || btnText ==locBigShip3 || btnText ==locBigShip4) {
        sB = true;
    }

    if (sB==true) {
    qInfo()<<"-------";
    qInfo()<<buttonSender->text();
    qInfo()<<"-------";}
    if (sB==false) {
        buttonSender->setEnabled(false);
        qInfo()<<"no ship";
        QPixmap pixmap(":/Game1Files/miss.png");
        QIcon ButtonIcon(pixmap);
        buttonSender->setIcon(ButtonIcon);
        buttonSender->setIconSize(pixmap.rect().size());

    }


}

void game1scene::setInternalLayout() {
    internalGridLayout->addWidget(a11,0,0);
    internalGridLayout->addWidget(a12,0,1);
    internalGridLayout->addWidget(a13,0,2);
    internalGridLayout->addWidget(a14,0,3);
    internalGridLayout->addWidget(a21,1,0);
    internalGridLayout->addWidget(a22,1,1);
    internalGridLayout->addWidget(a23,1,2);
    internalGridLayout->addWidget(a24,1,3);
    internalGridLayout->addWidget(a31,2,0);
    internalGridLayout->addWidget(a32,2,1);
    internalGridLayout->addWidget(a33,2,2);
    internalGridLayout->addWidget(a34,2,3);
    internalGridLayout->addWidget(a41,3,0);
    internalGridLayout->addWidget(a42,3,1);
    internalGridLayout->addWidget(a43,3,2);
    internalGridLayout->addWidget(a44,3,3);
}


